Private file token

Description:
------------
Provides access to private files and images by injecting authentication
token into their urls. Such a token will be validated when such url is accessed
and in case of successful validation the access will be granted.

Features:
---------

### 1. Expiration time.

The module allows to set an expiration time (in seconds) for which the token
will be considered valid. The default value has been set to three hours.

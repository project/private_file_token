<?php

namespace Drupal\Tests\private_file_token\Functional;

use Drupal\Core\File\FileSystemInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the private image access using private_file_token.
 *
 * @group private_file_token
 */
class PrivateFileTokenTest extends BrowserTestBase {

  use PrivateFileTokenTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'image',
    'private_file_token',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The private file token generator.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $tokenGenerator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->fileSystem = \Drupal::service('file_system');
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->tokenGenerator = \Drupal::service('private_file_token');
  }

  /**
   * Tests the access to the image style of a private image.
   */
  public function testPrivateImageAccess(): void {
    // Copy source test image into private files directory.
    $image_filename = 'private-file-token-test-image.png';
    $source_image_path = \Drupal::service('extension.list.module')->getPath('private_file_token') . '/tests/image/test-image.png';
    $this->fileSystem->copy($source_image_path, 'private://' . $image_filename, FileSystemInterface::EXISTS_REPLACE);
    // Create a test image file entity.
    $values = [
      'status' => 0,
      'filename' => $image_filename,
      'uri' => 'private://' . $image_filename,
      'filesize' => filesize($source_image_path),
    ];
    $storage = $this->entityTypeManager->getStorage('file');
    /** @var \Drupal\file\FileInterface $fileEntity */
    $image_entity = $storage->create($values);
    $image_entity->save();

    // Generate image style url of private image.
    $image_style = ImageStyle::load('thumbnail');
    $image_style_url = $image_style->buildUrl($image_entity->getFileUri());

    // Assert generated image style url.
    $expected_image_style_uri = '/system/files/styles/thumbnail/private/private-file-token-test-image.png';
    $this->assertPrivateImageStyleUrl($expected_image_style_uri, $image_style_url);

    // Access private image style url. We expect to get an image with 10 x 10
    // dimensions.
    $content = $this->drupalGet($image_style_url);
    [$width, $height, $type] = @getimagesizefromstring($content);
    $this->assertTrue($width == 10 && $height == 10 && image_type_to_mime_type($type) == 'image/png');

    // Change expiration time to zero and check that image has expired and
    // access is denied.
    \Drupal::configFactory()->getEditable('private_file_token.settings')
      ->set('expiration_time', 0)
      ->save();
    $content = $this->drupalGet($image_style_url);
    $this->assertContains('Access denied', $content);
  }

}

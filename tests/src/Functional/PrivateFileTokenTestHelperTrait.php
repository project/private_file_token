<?php

namespace Drupal\Tests\private_file_token\Functional;

use Drupal\Component\Utility\UrlHelper;

/**
 * Helper test class with some added functions for testing.
 */
trait PrivateFileTokenTestHelperTrait {

  /**
   * Asserts image style url of private image.
   *
   * @param string $expected_image_style_uri
   *   Expected image style uri.
   * @param string $image_style_url
   *   Actual image style url of private image to be checked.
   */
  public function assertPrivateImageStyleUrl(string $expected_image_style_uri, string $image_style_url): void {
    // Checks that generated url has expected path and contains expected query
    // string arguments.
    $image_style_url_parts = UrlHelper::parse($image_style_url);
    $path_part = \Drupal::service('file_url_generator')->transformRelative($image_style_url_parts['path']);

    $this->assertEquals($expected_image_style_uri, $path_part);
    $this->assertTrue(!empty($image_style_url_parts['query']['token']) && !empty($image_style_url_parts['query']['token']));
  }

}
